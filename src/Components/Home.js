import React from 'react';
import {connect} from 'react-redux';

import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";

const Home = (props) => {

    let categoryDisplay = () => {
        let categoryMap = props.category.map(a => {
            return <div className = "homeLinks">
                        <div className = "categoryLinks">{a.name}</div>
                   </div>
        })
    }
    
    return(
        <div className = "homeContainer clearfix">
            <div className = "navHome">
                {categoryDisplay()}
            </div>

            <div className = "carouselContainer">
                <Carousel autoPlay>
                    <div className = "carouselImg">
                        <img src = "https://i.pinimg.com/564x/49/82/65/498265cbd88c70212f630e8099be2033.jpg"/>
                    </div>

                    <div className = "carouselImg">
                        <img src = "https://i.pinimg.com/564x/d5/bd/e4/d5bde47fd34b0fa02847d745873c1031.jpg"/>
                    </div>

                    <div className = "carouselImg">
                        <img src = "https://i.pinimg.com/564x/d4/ea/29/d4ea299940a646f6e1b8a66fe019ef24.jpg"/>
                    </div>

                    <div className = "carouselImg">
                        <img src = "https://i.pinimg.com/564x/a8/e3/e2/a8e3e27f12757ae05a815f1d16d1ba8f.jpg"/>
                    </div>

                    <div className = "carouselImg">
                        <img src = "https://i.pinimg.com/564x/9c/6f/bd/9c6fbd110f8cd3f7d0ae6194ef1641a4.jpg"/>
                    </div>

                    <div className = "carouselImg">
                        <img src = "https://lh6.googleusercontent.com/proxy/eqm_jpovaTToZ4xb9EVh1rRBX4PCWXG4svnus-W9lTKnIx5t2HhVLT9NDzsUDFwWykP6_0RCAZgCoXgGljKuJ_vtYEkSet0j_mNB5-NmPacSmL_qgmBWsz_f1zShO4gCkySoSBoswgpLR2BCVXP3sHuU4PPHZkUUfHqOnVLe7mcwrw"/>
                    </div>

                    <div className = "carouselImg">
                        <img src = "https://files.thehandbook.com/uploads/2019/07/scotch-egg-beara-beara-1-960x570.jpg"/>
                    </div>

                    <div className = "carouselImg">
                        <img src = "https://66.media.tumblr.com/tumblr_mca50dw2qh1qitz6do1_500.jpg"/>
                    </div>

                    <div className = "carouselImg">
                        <img src = "https://lh4.googleusercontent.com/proxy/3KvG-JK0BAjo4mMec_QO1etTOZBddkZkzbh3JEUKHgzk-W6gPBpks7DQTzW1qs7s0gFl85ohKYTiVVxQq0ynUKfRzyby_6NOw7xjl_1kClVmTu9JtOgrKiN7-dZk6w"/>
                    </div>

                    <div className = "carouselImg">
                        <img src = "https://66.media.tumblr.com/5f380d70650c500023bfd010995a8a4c/tumblr_mj6gbo1v331qitz6do1_500.jpg"/>
                    </div>
                </Carousel>
            </div>

        </div>
    )
}

const mapStateToProps = (state) => {
    return {category : state.category}
}

export default connect(mapStateToProps)(Home);
