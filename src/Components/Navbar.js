import React from 'react';
import Navitem from './Navitem';
import logo from '../logo.png';

class Navbar extends React.Component{
    constructor(props)
    {
        super(props);
        this.state={
            'NavItemActive':''
        }
    }
    activeitem=(x)=>
    {
        if(this.state.NavItemActive.length>0){
            document.getElementById(this.state.NavItemActive).classList.remove('active');
        }
        this.setState({'NavItemActive':x},()=>{
            document.getElementById(this.state.NavItemActive).classList.add('active');
        });
    };

    render(){
        return(
            <div className = "navBar clearfix">

                    <div className = "topNav clearfix">
                        <div className = "socMedContainer clearfix">
                            <img src = "https://image.flaticon.com/icons/svg/1384/1384053.svg" className = "socmed"/>
                            <img src = "https://image.flaticon.com/icons/svg/2111/2111463.svg" className = "socmed"/>
                            <img src = "https://image.flaticon.com/icons/svg/1384/1384060.svg" className = "socmed"/>
                            <img src = "https://image.flaticon.com/icons/svg/733/733579.svg" className = "socmed"/>
                        </div>

                        <div className = "logoContainer">
                            <img src = {logo} id = "logo"/>
                        </div>

                        <div className = "loginBtnContainer">
                            <img src = "https://image.flaticon.com/icons/svg/1828/1828506.svg" id = "login"/>
                        </div>

                    </div>
               

                <div className = "navContainer clearfix">
                    <div className = "linkContainer clearfix">
                        <Navitem item="HOME" tolink="/"  activec={this.activeitem} className = "navLinkContainer"></Navitem>
                        <Navitem item="ABOUT" tolink="/about"  activec={this.activeitem} className = "navLinkContainer"></Navitem>
                        <Navitem item="BASICS" tolink="/basics"  activec={this.activeitem} className = "navLinkContainer" ></Navitem>
                        <Navitem item="TOP RECIPES" tolink="/top"  activec={this.activeitem} className = "navLinkContainer" ></Navitem>
                        <Navitem item="CONNECT" tolink="/connect"  activec={this.activeitem} className = "navLinkContainer"></Navitem>
                    </div>

                    <div className = "searchContainer clearfix">
                        <input type = "text" placeholder = "I want to make..." id = "search"/>
                        <button type = "button" id = "searchBtn">
                            <img src = "https://image.flaticon.com/icons/svg/620/620267.svg" id = "whisk"/>
                        </button>
                    </div>
                </div>


            </div>
        )
    }
}

export default Navbar;