const initialState = {
    category : [],
    // isLoggedIn : localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : false
}

const rootReducer = (state = initialState, action) => {

    // switch(action.type) {
    //     case 'LOGIN':
    //         return {
	// 			isLoggedIn: action.payload
	// 		}
	// 	case 'LOGOUT':
	// 		return {
	// 			isLoggedIn: false
	// 		}
    // }

    if (action.type === "CATEGORY_DATA"){
        let temp = action.payload
        // console.log(action.payload);
        return { category : temp }
    }

    return state
}

export default rootReducer;