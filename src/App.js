import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import './App.css';

import Navbar from './Components/Navbar';
import Home from './Components/Home';
import Recipes from './Components/Recipes';
// import Login from './Components/Login';
import About from './Components/About';
import Basics from './Components/Basics';
import Connect from './Components/Connect';

const axios = require('axios')

const dataBaseHandler = (data) => {
   return {
     type: "CATEGORY_DATA",
     payload: data
   }
}

class App extends React.Component{

  componentDidMount(){
    axios.get('http://localhost:8080/category')
    .then(res => {
      let newData = res.data;
      this.props.addToState(newData)
    });
  }

  render(){
    return(
      <Router>
      <div className = "App">
        <Navbar/>  

        <Route exact path="/">
          <Home />
        </Route>

        <Route path="/about">
          <About />
        </Route>    

        <Route path = "/basics">
          <Basics/>
        </Route>

        <Route path="/top">
          <Recipes />
        </Route>  

        <Route path="/connect">
          <Connect />
        </Route>  

        <footer>My Recipe, Your Recipe. 2020 © All Rights Reserved.</footer>

      </div>
      </Router>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addToState : (newData) => {dispatch(dataBaseHandler(newData))}

  }
}

export default connect(null, mapDispatchToProps)(App);

